<?php
/**
 *
 * Template Single
 * Página padrão para detalhe do post
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/
get_header();
$current_id = get_the_ID();
global $post;
?>
	<?php if ( have_posts() ): while ( have_posts() ): the_post();
	?>
	<article id="single" class="content">
		<header id="page-header">
			<div class="container">
				<div class="row">
                    <div class="col-md-5">
                        <h1 class="sr-only"><?php the_title();?></h1>
                        <div id="breadcrumbs">
                            <?php
                            if ( function_exists('yoast_breadcrumb') )
                                yoast_breadcrumb();
                            ?>
                        </div>
                    </div>
                </div>
			</div>
		</header>

		<section class="content">

			<div class="container">
				<div class="row texto-destaque">
					<div class="col-md-8">
                        <?php
                        the_title()
                        ?>
                        <hr class="separator">
                        <?php
                            the_excerpt()
                        ?>
                        <hr class="separator">
					</div>
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div id="page-content">
                             <?php echo(the_date("d/m/Y", "", " $nbsp | $nbsp") . get_the_author());?>

                            <p> &nbsp; </p>

                            <?php the_content(); ?>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <?php get_sidebar('default'); ?>
                    </div>
                </div>
            </div>
		</section>
	</article><!-- fim do .content -->
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
