<?php
/**
 *
 * Inclui o tipo de post "Case"
 *
 * Version: 1.0
 * Author: Rhenato Francisco Baraúna
 * Author URI: https://www.linkedin.com.br/rhbarauna
 *
 **/

function register_produto_type() {

    $labels = array(
        'name'               => __( 'Cadeiras' ),
        'singular_name'      => __( 'Cadeiras' ),
        'menu_name'          => __( 'Cadeiras' ),
        'name_admin_bar'     => __( 'Cadeiras' ),
        'add_new'            => __( 'Adicionar nova' ),
        'add_new_item'       => __( 'Adicionar nova cadeira' ),
        'new_item'           => __( 'Nova cadeira' ),
        'edit_item'          => __( 'Editar cadeira' ),
        'view_item'          => __( 'Ver cadeira' ),
        'all_items'          => __( 'Todas as Cadeiras' ),
        'search_items'       => __( 'Buscar cadeira' ),
        'not_found'          => __( 'Nenhum cadeira encontrada.' ),
        'not_found_in_trash' => __( 'Nenhum cadeira encontrada na lixeira.' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => true,
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'produto', $args );

    $labels_cats = array(
        'name'              => __( 'Categorias', TEXT_DOMAIN ),
        'singular_name'     => __( 'Categoria', TEXT_DOMAIN ),
        'search_items'      => __( 'Buscar Categorias' ),
        'all_items'         => __( 'Todos os Categorias' ),
        'parent_item'       => __( 'Categorias Pai' ),
        'parent_item_colon' => __( 'Categorias Pai:' ),
        'edit_item'         => __( 'Editar Categoria' ),
        'update_item'       => __( 'Atualizar Categoria' ),
        'add_new_item'      => __( 'Adicionar nova Categoria' ),
        'new_item_name'     => __( 'Nome da nova Categoria' ),
        'menu_name'         => __( 'Categorias' ),a
    );

    $tax_args = array(
        'hierarchical'      => true,
        'labels'            => $labels_cats,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'cadeiras' ),
    );

    register_taxonomy( 'linhas', array( 'produto' ), $tax_args );
}
add_action( 'init', 'register_produto_type' );