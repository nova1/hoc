<?php
/**
 *
 * Template para exibição de post único
 *
 * Version: 1.0
 * Author: Infinito Web Sites
 *
 **/
?>

<article class="post post-item">
    <header id="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div id="breadcrumbs">
                        <?php
                        if ( function_exists('yoast_breadcrumb') )
                            yoast_breadcrumb();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="content">
        <div class="container">
            <div id="page-content">

            </div>
            <p>&nbsp;</p>
        </div>
    </section><!-- fim do .content-body -->

</article>