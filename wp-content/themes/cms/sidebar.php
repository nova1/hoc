<?php
/**
 *
 * Sidebar
 * Modelo padrão de barra lateral
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/



?>
<div id="sidebar">
   <ul>
      <?php
      if ( function_exists('dynamic_sidebar') && dynamic_sidebar('default') ) :?>
          <img src=" <?php the_field('author_image', 'option');?>" />

          <p>&nbsp;</p>

          <div>
            <?php the_field('author_profile', 'option');?>
          </div>
      <?php endif; ?>
</ul>
</div>