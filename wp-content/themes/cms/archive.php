<?php
/**
 *
 * Version: 1.0
 * Author: Rhenato Francisco Baraúna
 * Author URI: https://www.linkedin.com/rhbarauna
 *
 **/
get_header(); ?>

	<article id="blog" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div id="breadcrumbs">
                            <?php
                            if ( function_exists('yoast_breadcrumb') )
                                yoast_breadcrumb();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="container">
                <div id="page-content">
                    <div class="row">
                        <?php
                            $query_args = array(
                                'posts_per_page'    => 1,
                                'post_status'       => 'publish',
                                'post_type'         => 'post',
                                'include'			=> [get_field('post_principal', 'option')]
                            );
                            $query = new WP_Query($query_args);

                            if( $query->have_posts() ):
                                while( $query->have_posts() ): $query->the_post(); ?>
                                    <a href="<?php the_permalink(); ?>">

                                        <div class="col-3" id="blog-image" style='background-image : "<?php the_field('image');?>;"' >
                                            <span id="blog-image-text">
                                                <?php the_title(); ?>
                                            </span>
                                        </div>

                                    </a>
                                <?php endwhile;
                                wp_reset_query();
                                wp_reset_postdata();
                            endif;
                        ?>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">

                        <?php
                            $query_args = array(
                                'posts_per_page'    => 7,
                                'post_status'       => 'publish',
                                'post_type'         => 'post',
                                'include'			=> [get_field('selected_posts', 'option')]
                            );

                            $query = new WP_Query($query_args);

                            if( $query->have_posts() ):
                                while( $query->have_posts() ): $query->the_post(); ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="col-3" id="blog-image" style='background-image: url(<?php the_field('image');?>)' >

                                            <span id="blog-image-text">
                                                <?php the_title(); ?>
                                            </span>

                                            <p>&nbsp;</p>

                                            <?php the_excerpt()?>

                                        </div>
                                    </a>
                                <?php endwhile;
                                wp_reset_query();
                                wp_reset_postdata();
                            endif;
                         ?>
                    </div>
				</div>
                <p>&nbsp;</p>
			</div>
		</section><!-- fim do .content-body -->		
	</article><!-- fim do .content -->

<?php get_footer(); ?>