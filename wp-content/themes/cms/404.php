<?php
/**
 *
 * Template 404
 * Exibição de mensagem para páginas não encontradas
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/
get_header(); 
?>
	<article  class="content">
		<header id="page-header">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<h1 class="page-title">
							Página não encontrada
						</h1>						
					</div>

					<div class="col-md-5">
						<div id="breadcrumbs">
						<?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb(); ?>							
						</div>
					</div>
				</div>
			</div>
		</header>

		<section class="content">
			<div class="container">
				<p>O conteúdo que você está procurando não existe ou pode ter sido removido.<br>
				Retorne a navegação clicando no botão abaixo:</p>
				
				<p>&nbsp;</p>
				
				<p><a href="<?php bloginfo( 'url' ); ?>" class="btn btn-danger">Voltar à Home</a></p>
			</div>
		</section><!-- fim do .content-body -->
	</article><!-- fim do .page -->	
<?php get_footer(); ?>