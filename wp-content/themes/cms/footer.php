<?php
/**
 * Template do Rodapé
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
?>

</div><!-- /.main -->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div id="footer-logo">
                    <a href="<?php bloginfo('url') ?>" title="<?php bloginfo('name') ?>">
                        <img src="<?php bloginfo('template_url'); ?>/img/logo_rodape.png" alt="<?php bloginfo('name'); ?>">
                    </a>
                </div>
            </div>

            <div class="col-md-6 d-none d-sm-block">
                <nav id="bottom-menu" role="navigation">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'full_menu',
                        'menu_class' => 'nav nav-pills',
                        'container' => false,
                        'walker' => new wp_bootstrap_navwalker()
                    )); ?>
                </nav>
            </div>

            <div class="col-md-2">
                <div class="social">
                    <?php if (get_field('facebook', 'option')): ?>
                        <a href="<?php the_field('facebook', 'option') ?>" target="_blank">
                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>

                    <?php if (get_field('linkedin', 'option')): ?>
                        <a href="<?php the_field('linkedin', 'option') ?>" target="_blank">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.container -->

    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <small>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. Todos os Direitos Reservados.
                    </small>
                </div>
                <div class="col-md-6">
                    <div class="text-right">
                        <img src="<?php bloginfo('template_url'); ?>/img/rodape_neocom.png"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- end #footer -->
<?php wp_footer(); ?>
<script type="application/javascript" href="<?php bloginfo('template_url'); ?>/main.js"></script>
</body>
</html>