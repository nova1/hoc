<?php
/**
 * Template do Cabeçalho
 * Exibe tudo na seção <head> e início do <body> até o <article>
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- monta o titulo da página -->

    <title><?php wp_title(); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Vidaloka" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="loading"></div>
<div id="menu-suspenso" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-sm menu-logo">
                <img src="<?php bloginfo('template_url'); ?>/img/logotipo_menu.png" alt="<?php bloginfo('name'); ?>">
            </div>
            <div class="col-sm meio">
                <?php wp_nav_menu(array(
                    'theme_location' => 'menu_topo',
                    'menu_class' => 'nav navbar-nav',
                    'container' => false,
                    'walker' => new wp_bootstrap_navwalker()
                )); ?>
            </div>
            <div class="col-sm menu-back">
                <span onclick="hideMenu()">Voltar <img src="<?php bloginfo('template_url'); ?>/img/seta_breadcrumb.png"></span>

            </div>
        </div>
    </div>
</div>
<header id="header">
        <nav class="navbar fixed-top ">
            <a class="navbar-brand" href="<?php echo home_url(); ?>">
                <img src="<?php bloginfo('template_url'); ?>/img/logo_topo.png" alt="<?php bloginfo('name'); ?>">
            </a>
            <span class="navbar-toggler-icon" onclick="showMenu()">
            <img src="<?php bloginfo('template_url'); ?>/img/icone_menu_linhas.png" alt="<?php bloginfo('name'); ?>">
        </span>
        </nav>

</header><!-- fim do #header -->

<div class="main">
		


					