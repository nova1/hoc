<?php
/**
 *
 * Template name: Sobre
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header(); ?>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <article id="sobre" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <h1 class="sr-only"><?php the_title();?></h1>
                    <div id="breadcrumbs">
                        <?php
                        if (function_exists('yoast_breadcrumb'))
                            yoast_breadcrumb();
                        ?>
                    </div>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="container">
                <div id="page-content">
                    <div class="row">
                        <div class="col-lg-10">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <p>&nbsp;&nbsp;</p>

            <div class="mr-auto ml-auto">
                <?php if (get_field('middle_image')): ?>
                    <img src="<?php the_field('middle_image') ?>" alt="">
                <?php endif; ?>
            </div>

            <p>&nbsp;</p>

            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <?php if (get_field('showroom_text')):
                            the_field('showroom_text');
                        endif; ?>
                    </div>
                </div>

                <p>&nbsp;</p>

                <div class="row">
                    <div class="col-lg-10">
                        <?php
                        $products = get_field('our_products');
                        ?>
                        <div class="row">
                            <?php
                            foreach ($products as $product):?>
                                <div class="col-lg-3">
                                    <img src="<?= $product['url']?>" />
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <p>&nbsp;</p>

            </div>
        </section><!-- fim do .content-body -->
    </article><!-- fim do .content -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>