<?php
/**
 *
 * Template Single
 * Página padrão para detalhe do post
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/
get_header();
$current_id = get_the_ID();
?>
	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

	<article id="single" class="content">
		<header id="page-header">
			<div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <h1 class="sr-only"><?php the_title();?></h1>
                        <div id="breadcrumbs">

                            <?php
                            if ( function_exists('yoast_breadcrumb') )
                                yoast_breadcrumb();
                            ?>
                        </div>
                    </div>
                </div>
		</header>

        <section class="content">
            <div class="container">
                <div id="page-content">

                    <div class="row">
                        <div class="col-lg-10">
                            <?php if (get_field('page_content')): ?>
                                <?php the_field('page_content') ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">
                        <a href="#">
                            <?php
                            the_field('case_texto_link_botao_interno', 'option') ?>
                            <img src="<?php bloginfo('template_url'); ?>/img/seta_dropdown.png" />
                        </a>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">
                        <div class="col-6">
                            <?php the_field('item_description');?>
                        </div>
                        <div class="col-6">
                            <?php the_field('item_images');?>
                        </div>
                    </div>

                    <?php
                    $query_args = array(
                        'posts_per_page'    => 4,
                        'post_status'       => 'publish',
                        'post_type'         => 'case',
                        'orderby'			=> 'modified',
                        'order'				=> 'DESC',
                        'post__not_in'      => array( $current_id )
                    );

                    $query = new WP_Query($query_args);

                    if( $query->have_posts() ):?>
                        <p>&nbsp;</p>
                            <h2>Connheça outros Cases</h2>
                        <p>&nbsp;</p>
                        <div class="row">
                            <?php while( $query->have_posts() ): $query->the_post();
                                ?>
                                <a href="<?php the_permalink(); ?>">
                                    <div class="col-lg-3">
                                        <?php the_field('imagem_item') ?>
                                    </div>

                                    <div class="link">
                                        <?php the_title();?>
                                    </div>
                                </a>
                            <?php endwhile; ?>
                        </div>
                    <?php endif;
                    wp_reset_query();
                    wp_reset_postdata();
                    ?>
                </div>
            </div>

            <p>&nbsp;</p>

            <div class="pink-background">
                <?php get_template_part('partial-blog-line')?>
            </div>

        </section>
	</article><!-- fim do .content -->
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
