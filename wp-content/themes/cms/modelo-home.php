<?php
/**
 *
 * Template name: Home
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header(); ?>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <article id="home" class="content">
        <h1 class="sr-only"><?php the_title();?></h1>

        <div id="slider-row">
            <div id="home_slider">
                <?php if (function_exists('cyclone_slider')) cyclone_slider('home'); ?>
            </div>
        </div>

        <div id="home-our-products-row" class="container">

            <p>&nbsp;</p>

            <div>
                <?php the_content(); ?>
            </div>

            <p>&nbsp;</p>

            <div class="row home-products">
                <?php

                if( get_field('products_repeater')):
                    $products_repeater = get_field('products_repeater');

                    if($products_repeater[0]['products']):

                        $query_args = array(
                            'posts_per_page'    => 4,
                            'post_status'       => 'publish',
                            'post_type'         => 'produto',
                            'orderby'			=> 'modified',
                            'include'           => $products_repeater[0]['products']
                        );

                        $query = new WP_Query($query_args);

                        if( $query->have_posts() ):
                            while( $query->have_posts() ): $query->the_post();
                                $repeater = get_field('repeater');
                                ?>

                                <a href="<?php the_permalink(); ?>"  class="link">
                                    <div class="col-sm home-products-col">
                                        <div class="row">
                                            <img src="<?= $repeater[0]['images'][0]['url']; ?>" />
                                        </div>

                                        <div class="row">
                                            <span><?php the_title(); ?></span>
                                        </div>

                                        <div class="row">
                                            <p><?php the_excerpt(); ?></p>
                                        </div>
                                    </div>
                                </a>
                            <?php endwhile;
                        endif;
                        wp_reset_query();
                        wp_reset_postdata();
                    endif;
                endif; ?>
            </div>
        </div>
        <div class="home-showcase container">
            <div class="row">
                <?php

                if( get_field('cases_repeater')):
                    $cases_repeater = get_field('cases_repeater');

                    if($cases_repeater[0]['cases']):

                        $query_args = array(
                            'posts_per_page'    => 2,
                            'post_status'       => 'publish',
                            'post_type'         => 'case',
                            'orderby'			=> 'modified',
                            'include'           => $cases_repeater[0]['cases']
                        );

                        $query = new WP_Query($query_args);

                        if( $query->have_posts() ):
                            while( $query->have_posts() ): $query->the_post();
                            $image_url = get_field('item_images')[0]['url'];
                        ?>

                                <a href="<?php the_permalink(); ?>"  class="link">
                                    <div class="col-sm home-products-col">
                                        <div class="row img-case" style="background-image: url(<?= $image_url; ?>)"></div>

                                        <div class="row">
                                            <span><?php the_title(); ?></span>
                                        </div>

                                        <div class="row">
                                            <p><?php the_excerpt(); ?></p>
                                        </div>
                                    </div>
                                </a>
                            <?php endwhile;
                        endif;
                        wp_reset_query();
                        wp_reset_postdata();
                    endif;
                endif; ?>
            </div>
        <!--                    <div class="row">-->
        <!--                        --><?php
        //                        $selected_products = get_field('selected_products');
        //
        //                        $query_args = array(
        //                            'post_status' => 'publish',
        //                            'post_type' => 'produto',
        //                            'include'=> $selected_products
        //                        );
        //
        //                        $query = new WP_Query($query_args);
        //
        //                        if ($query->have_posts()):
        //                            while ($query->have_posts()):
        //                                $query->the_post(); ?>
        <!--                                <a href="--><?php //the_permalink(); ?><!--" class="col-sm-3 item-showcase">-->
        <!--                                    <div class="cadeira">-->
        <!--                                        <div class="thumb">-->
        <!--                                            --><?php
        //                                            $images = get_field('images')?>
        <!---->
        <!--                                            <img src="--><?//= $image[0]['url'];?><!--">-->
        <!--                                            <p>--><?php //the_title(); ?><!--</p>-->
        <!--                                        </div>-->
        <!--                                    </div>-->
        <!--                                </a>-->
        <!--                            --><?php //endwhile;
        //                        endif;
        //                        wp_reset_postdata();
        //                        wp_reset_query();
        //                        ?>
        <!--                    </div>-->
        </div>
        <div class="fluid home-red-belt">

            <div class="container red-belt-content">
                <hr>
                <div class="row blog">
                    <?php get_template_part('partial-blog-line')?>
                </div>
            </div>

        </div>
    </article>
<?php endwhile; endif; ?>
<?php get_footer(); ?>