<?php
/**
 *
 * Template name: Contato
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
 **/
get_header(); ?>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <article id="contato" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <h1 class="sr-only"><?php the_title();?></h1>
                    <div id="breadcrumbs">
                        <?php
                        if (function_exists('yoast_breadcrumb'))
                            yoast_breadcrumb();
                        ?>
                    </div>
                </div>
            </div>
        </header>

        <section class="content">
            <div id="page-content">
                <div class="row">
                    <div class="col-lg-10">
                        <?php if (get_field('page_content')): ?>
                            <?php the_field('page_content') ?>
                        <?php endif; ?>
                    </div>
                </div>

                <p>&nbsp;</p>

                <div class="row">
                    <?php the_field('botao_opcoes') ?>
                    <img src="<?php bloginfo('template_url'); ?>/img/seta_dropdown.png" />
                </div>

                <p>&nbsp;</p>

                <div class="row">
                    <div class="col-lg-12 forms-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6" id="form-contato">
                                    <?php the_field('texto_contato') ?>
                                    <?php echo do_shortcode('[contact-form-7 id="6" title="Formulário de contato"]') ?>
                                </div>

                                <div class="col-lg-6">
                                    <div id="page-sidebar" class="sidebar-right">
                                        <?php the_field('conteudo_contato') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- fim do .content-body -->
    </article><!-- fim do .content -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>