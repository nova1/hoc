<?php
/**
 *
 * Version: 1.0
 * Author: Rhenato Francisco Baraúna
 * Author URI: https://www.linkedin.com/rhbarauna
 *
**/
get_header(); ?>

    <article id="posts" class="content">
        <header id="page-header">
            <div class="container">
                <div class="row">
                    <div id="breadcrumbs">
                        <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb(); ?>
                    </div>
                </div>
            </div>
        </header>

        <section class="content">
            <div class="container">
                <div id="page-content">

                    <div class="row">
                        <div class="col-lg-10">
                            <?php
                             if (get_field('case_page_content', 'option')):
                                the_field('case_page_content', 'option');
                            endif; ?>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">
                        <a href="#">
                            <?php
                            the_field('case_texto_link_botao', 'option') ?>
                            <img src="<?php bloginfo('template_url'); ?>/img/seta_dropdown.png" />
                        </a>
                    </div>
                    <p>&nbsp;</p>
                    <div class="row">
                    <?php
                        $query_args = array(
                            'post_status'       => 'publish',
                            'post_type'         => 'case',
                            'orderby'			=> 'modified',
                            'order'				=> 'DESC'
                        );

                        $query = new WP_Query($query_args);

                        if( have_posts() ):
                            while( have_posts() ): the_post();
                        ?>
                                <a href="<?php the_permalink(); ?>">
                                    <div class="col-lg-3">
                                        <?php
                                        $repeater = get_field('repeater');
                                        $images = $repeater[0]['images'];
                                        ?>
                                        <img src="<?= $images[0]['url']; ?>" />
                                    </div>

                                    <div class="link">
                                        <?php the_title();?>
                                    </div>
                                </a>
                    <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>

            <p>&nbsp;</p>

            <div class="pink-background">
                <?php get_template_part('partial-blog-line')?>
            </div>
        </section><!-- fim do .content-body -->
    </article><!-- fim do .content -->
<?php get_footer(); ?>