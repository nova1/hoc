<?php
/**
 *
 * Template Single
 * Página padrão para detalhe do post
 *
 * Version: 1.0
 * Author: InfinitoAG
 * Author URI: http://www.infinitoag.com
 *
**/
get_header();
$current_id = get_the_ID();
?>
	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

	<article id="single" class="content">
		<header id="page-header">
			<div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <h1 class="sr-only"><?php the_title();?></h1>
                        <div id="breadcrumbs">

                            <?php
                            if ( function_exists('yoast_breadcrumb') )
                                yoast_breadcrumb();
                            ?>
                        </div>
                    </div>
                </div>
		</header>

        <section class="content">
            <div class="container">
                <div id="page-content">

                    <div class="row">
                        <div class="col-lg-10">
                            <?php if (get_field('page_content')): ?>
                                <?php the_field('page_content') ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">
                        <a href="#">
                            <?= the_field('produto_texto_link_botao_interno', 'option');?> &nbsp;<?=the_field('linha_do_produto'); ?>
                            <img src="<?php bloginfo('template_url'); ?>/img/seta_dropdown.png" />
                        </a>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">
                        <div class="col-12">
                            <?php
                                $produtos = get_field('repeater');
                                $counter = 0;
                                foreach ($produtos as $produto):
                                    $class = "";

                                    if($counter == 0 ){
                                        $class = "active";
                                    }?>

                                    <div class="row <?= $class;?>">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-6">
                                                    <?= $produto['description'];?>

                                                    <p>&nbsp;</p>

                                                    <a href="<?= $produto['product_manual'];?>">Baixe o manual do produto</a>
                                                </div>

                                                <div class="col-6">
                                                    <?php $images = $produto['images'];
                                                    $counter = 0;?>

                                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                                        <ol class="carousel-indicators">
                                                        <?php foreach ($images as $image):
                                                            $class = "";

                                                            if($counter == 0 ){
                                                                $class = "active";
                                                            }
                                                            ?>

                                                            <li data-target="#carouselExampleIndicators" data-slide-to="<?= $counter; ?>" class="<?= $class; ?>" >
                                                                <img src="<?= $image['url'];?>" />
                                                            </li>

                                                        <?php
                                                        $counter++;
                                                        endforeach; ?>

                                                        </ol>
                                                        <div class="carousel-inner">
                                                            <?php
                                                                $counter = 0;
                                                                foreach ($images as $image):
                                                                    $class = "";
                                                                    if($counter == 0 ){
                                                                        $class = "active";
                                                                    }
                                                            ?>
                                                                    <div class="carousel-item <?= $class?>">
                                                                        <img class="d-block w-100" src="<?= $image['url'];?>" />
                                                                    </div>
                                                                <?php
                                                                    $counter++;
                                                                endforeach; ?>
                                                        </div>

                                                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   </div>
                                <?php endforeach; ?>
                        </div>
                    </div>
                    <?php
                    $query_args = array(
                        'posts_per_page'    => 4,
                        'post_status'       => 'publish',
                        'post_type'         => 'produto',
                        'orderby'			=> 'modified',
                        'order'				=> 'DESC',
                        'post__not_in'      => array( $current_id )
                    );

                    $query = new WP_Query($query_args);

                    if( $query->have_posts() ):?>

                        <p>&nbsp;</p>

                        <h2>Connheça outros Produtos</h2>

                        <p>&nbsp;</p>

                        <div class="row">

                            <?php while( $query->have_posts() ): $query->the_post();
                                $repeater = get_field('repeater');
                            ?>
                            <a href="<?php the_permalink(); ?>">
                                <div class="col-lg-3">
                                    <img src="<?= $repeater[0]['images'][0]['url']; ?>" />
                                </div>

                                <div class="link">
                                    <?php the_title();?>
                                </div>
                            </a>
                            <?php endwhile; ?>
                        </div>
                    <?php endif;
                    wp_reset_query();
                    wp_reset_postdata();
                    ?>

                </div>
            </div>

            <p>&nbsp;</p>

            <div class="pink-background">
                <?php get_template_part('partial-blog-line')?>
            </div>

        </section>
	</article><!-- fim do .content -->
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
