<?php
/**
 * Created by PhpStorm.
 * User: rhenatobarauna
 * Date: 04/12/17
 * Time: 09:32
 */
?>
<div class = "container"
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-12">
            <div class="row">
                <h3>Blog</h3>
            </div>
            <div class="row">
                <?php the_field('bottom_archive_blog_excerpt', 'option') ?>
            </div>
            <div class="row">
                <?php
                if( get_field('selected_posts', 'option')):
                    $query_args = array(
                        'posts_per_page'    => 4,
                        'post_status'       => 'publish',
                        'post_type'         => 'post',
                        'orderby'			=> 'modified',
                        'order'				=> 'DESC',
                        'include'           => get_field('selected_posts')
                    );

                    $query = new WP_Query($query_args);

                    if( $query->have_posts() ):
                        while( $query->have_posts() ): $query->the_post();
                            ?>
                            <a href="<?php the_permalink(); ?>"  class="link">
                                <div class="col-lg-3">
                                    <?php the_title() ?>
                                </div>
                            </a>
                        <?php endwhile;
                    endif;
                    wp_reset_query();
                    wp_reset_postdata();
                endif; ?>
            </div>
        </div>
    </div>
</div>